import React from 'react'

function Task({ id, title, dueDate, completed, onClick }) {
    return (
        <div data-testid={`task-container-${id}`} className="task-container" onClick={() => onClick(id)}>
            <div data-testid="task-title">{title}</div>
            <div
                data-testid="task-dueDate"
                style={{
                    textAlign: 'right'
                }}>{dueDate.toLocaleDateString()}</div>
            {completed && <div className="strike-text" data-testid={`task-strike-${id}`} />}
        </div>
    )
}

export default Task