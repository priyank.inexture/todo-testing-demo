import { render, screen } from '@testing-library/react';
import Task from '../Task';
describe("Task unit test", () => {
    test('Task Component rendered without strike', () => {
        const mockCallBack = jest.fn(1);
        render(<Task onClick={mockCallBack} title="Task title" dueDate={new Date(2021, 4, 6, 0, 0)} id={1} completed={false} />);

        const taskElement = screen.getByTestId('task-container-1');
        const taskTitleElement = screen.getByTestId('task-title');
        const taskDueDateElement = screen.getByTestId('task-dueDate');
        expect(taskElement).toBeInTheDocument();
        expect(taskTitleElement).toBeInTheDocument();
        expect(taskDueDateElement).toBeInTheDocument();
        expect(taskTitleElement).toHaveTextContent(/Task title/i);
        expect(taskDueDateElement).toHaveTextContent('6/5/2021');
        // expect(taskElement).toHaveTextContent(/hello/i);
    });

    test('Task Component rendered with strike', () => {
        const mockCallBack = jest.fn(1);
        render(<Task onClick={mockCallBack} title="Task title" dueDate={new Date(2021, 4, 6, 0, 0)} id={1} completed={true} />);
        const taskElement = screen.getByTestId('task-container-1');
        const taskTitleElement = screen.getByTestId('task-title');
        const taskDueDateElement = screen.getByTestId('task-dueDate');
        const taskStikeElement = screen.getByTestId('task-strike-1');
        expect(taskElement).toBeInTheDocument();
        expect(taskTitleElement).toBeInTheDocument();
        expect(taskDueDateElement).toBeInTheDocument();
        expect(taskStikeElement).toBeInTheDocument();
        expect(taskTitleElement).toHaveTextContent(/Task title/i);
        expect(taskDueDateElement).toHaveTextContent('6/5/2021');
        // expect(taskElement).toHaveTextContent(/hello/i);
    });


})
