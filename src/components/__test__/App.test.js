import { render, screen, fireEvent, waitFor } from '@testing-library/react';
import userEvent from '@testing-library/user-event'
import { act } from 'react-dom/test-utils'
import App from '../../App'
test('Adding new Task and marking completed', async () => {
    const { getByTestId } = render(<App />)
    const appElement = screen.getByTestId("App");
    const titleInput = await waitFor(() => screen.getByLabelText(/title/i));
    const dateInput = await waitFor(() => screen.getByLabelText(/Due Date/));
    const submitButton = screen.getByRole("button", { name: "Add" });
    expect(appElement).toBeInTheDocument();
    expect(appElement).toHaveTextContent(/Title/);

    await userEvent.type(titleInput, 'testtask', { delay: 1 });
    await waitFor(() => fireEvent.change(dateInput, { target: { value: '2021-11-06' } }))
    await waitFor(() => fireEvent.click(submitButton));
    // screen.debug(appElement);
    expect(appElement).toHaveTextContent(/Due Date/);
    expect(appElement).toHaveTextContent('testtask');
    expect(appElement).toHaveTextContent('6/11/2021');

    const taskElement = screen.getByTestId('task-container-4');
    expect(() => getByTestId('task-strike-4')).toThrow();
    fireEvent.click(taskElement);
    expect(screen.getByTestId('task-strike-4')).toBeInTheDocument();
})