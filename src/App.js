import './App.css';
import Task from './components/Task';
import TaskSchema from './validations/TaskSchema'
import { Formik } from "formik";
import { useState } from 'react';
function App() {
  const today = new Date();
  today.setHours(0, 0, 1, 0);
  // console.log(`${today.getUTCFullYear()}-${(today.getMonth() < 9 ? '0' : '') + (today.getMonth() + 1)}-${(today.getDate() < 9 ? '0' : '') + (today.getDate())}`)
  const [tasks, setTasks] = useState([
    { id: 1, title: 'this is first Task', dueDate: new Date(2021, 4, 7), completed: false },
    { id: 2, title: 'this is second Task', dueDate: new Date(2021, 4, 7), completed: true },
    { id: 3, title: 'this is third Task', dueDate: new Date(2021, 4, 7), completed: false },
  ])
  const handleCompleteTask = (id) => {
    const task = tasks.find(t => t.id === id);
    if (task) {
      task.completed = !task.completed;
      setTasks(prevTasks => prevTasks.map(t => t !== id ? t : task))
    }
  }
  return (
    <div className="App" data-testid="App">
      <Formik
        data-testid="app-form"
        initialValues={{ title: "", dueDate: `${today.getUTCFullYear()}-${(today.getMonth() < 9 ? '0' : '') + (today.getMonth() + 1)}-${(today.getDate() < 9 ? '0' : '') + (today.getDate())}` }}
        onSubmit={(values, { setSubmitting, resetForm }) => {
          // console.log("new values", values);
          let id = -1;
          tasks.forEach(t => id < t.id ? id = t.id : null);
          id++;
          setTasks(prevTasks => [...prevTasks, { id: id, title: values.title, dueDate: new Date(values.dueDate), completed: false }])
          setSubmitting(false);
          resetForm();
        }}
        validationSchema={TaskSchema}
      >
        {props => {
          const {
            values,
            touched,
            errors,
            isSubmitting,
            handleChange,
            handleBlur,
            handleSubmit
          } = props;
          return (
            <form onSubmit={handleSubmit}>
              <label id="title">Title</label>
              <input
                name="title"
                type="text"
                placeholder="Enter Task"
                aria-labelledby="title"
                value={values.title}
                onChange={handleChange}
                onBlur={handleBlur}
                className={errors.title && touched.title && "error"}
              />
              {errors.title && touched.title && (
                <div className="input-feedback">{errors.title}</div>
              )}
              <label id="duedate">Due Date</label>
              <input
                name="dueDate"
                type="date"
                aria-labelledby="duedate"
                value={values.dueDate}
                onChange={handleChange}
                onBlur={handleBlur}
                className={errors.dueDate && touched.dueDate && "error"}
              />
              {errors.dueDate && touched.dueDate && (
                <div className="input-feedback">{errors.dueDate}</div>
              )}
              <button data-testid="submitButton" type="submit" disabled={isSubmitting}>
                Add
          </button>
            </form>
          );
        }}
      </Formik>
      <div style={{
        margin: '0 auto',
        maxWidth: '600px',
        marginTop: '50px',
        width: '100%'
      }}>
        {
          tasks.map(t => <Task {...t} key={t.id} onClick={handleCompleteTask} />)
        }
      </div>
    </div>
  );
}

export default App;
