import * as yup from 'yup'
const today = new Date();
today.setHours(0, 0, 0, 0)
const TaskSchema = yup.object().shape({
    title: yup
        .string()
        .min(2, "Title must be more than 2 characters")
        .max(50, "Title must be less than 50 characters")
        .required(),
    dueDate: yup
        .date()
        .min(today, "Date Cannot be in the past")
        .required()
})
export default TaskSchema;